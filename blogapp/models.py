from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class author(models.Model):
   name = models.ForeignKey(User,on_delete=models.CASCADE)
   def __str__(self):
       return self.name.username
class category(models.Model):
    category = models.CharField(max_length=100)
    def __str__(self):
        return self.category
class post(models.Model):
    post_author = models.ForeignKey(author,on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField()
    category = models.ForeignKey(category,on_delete=models.CASCADE)
    def __str__(self):
        return self.title
